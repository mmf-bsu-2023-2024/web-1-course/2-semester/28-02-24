﻿#include <iostream>
#include"Strings.h"
using namespace std;

int main()
{
	const char* source = "The  -98 standard chunk,, 987 of Lorem Ipsum used since the 1500 is reproduced below for those interested.";
	int n = 0;
	char** words = StringExtension::obtainWords(source, n);
	cout << endl << "Words: " << endl;
	StringExtension::displayWords(words, n);
	StringExtension::sortingWords(words, n);
	cout << endl << "Sorted words: " << endl;
	StringExtension::displayWords(words, n);
	system("pause");
	return 0;
}
// The output is:
//Words:
//1. The
//2. standard
//3. chunk
//4. of
//5. Lorem
//6. Ipsum
//7. used
//8. since
//9. the
//10. is
//11. reproduced
//12. below
//13. for
//14. those
//15. interested
//
//Sorted words :
//1. Ipsum
//2. Lorem
//3. The
//4. below
//5. chunk
//6. for
//7. interested
//8. is
//9. of
//10. reproduced
//11. since
//12. standard
//13. the
//14. those
//15. used