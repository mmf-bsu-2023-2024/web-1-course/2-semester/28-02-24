#pragma once

// The StringExtension namespace contains several methods for manipulating strings
namespace StringExtension
{
	using Comparer = int (*) (const char* str1, const char* str2);
	/**
	 * @brief Splits the surce string into words based on the specified symbols and returns an array of words
	 * @param source The source string to split
	 * @param n The number of words obtained from the source string
	 * @return An array of words obtained from the source string
	 */
	char** obtainWords(const char* source, int& n, const char* symbols);

	/**
	 * @brief Similar to obtainWords, but uses a different set of symbols for splitting the source string
	 * @param source The source string to split
	 * @param n The number of words obtained from the source string
	 * @return An array of words obtained from the source string
	 */
	char** obtainWordsAgain(char* source, int& n);

	/**
	 * @brief Prints the words in the given array to the console
	 * @param words The array of words to display
	 * @param n The number of words in the array
	 */
	void displayWords(char** words, int n);

	/**
	 * @brief Frees the memory allocated for the array of words
	 * @param words The array of words to free
	 * @param n The number of words in the array
	 */
	void freeHeap(char** words, int n);

	/**
	 * @brief Sorts the array of words in ascending order
	 * @param words The array of words to sort
	 * @param n The number of words in the array
	 */
	void sortingWords(char** words, int n, Comparer comparer);

	/**
	 * @brief Swaps the positions of two words in the array
	 * @param x The first word
	 * @param y The second word
	 */
	void swap(char*& x, char*& y);

	/**
	 * @brief Replaces all occurrences of a substring in a string with another string
	 * @param string The source string
	 * @param subString The substring to replace
	 * @param replaceString The string to replace the substring with
	 */
	void replaceSubString(char* string, char* subString, char* replaceString);

	/**
	 * @brief Deletes all occurrences of a substring in a string
	 * @param string The source string
	 * @param subString The substring to delete
	 */
	void deleteSubString(char* string, char* subString);

	/**
	 * @brief Deletes all words of a certain length from the source string
	 * @param source The source string
	 * @param length The length of the words to delete
	 */
	void deleteSubString(char* source, int length);

	char* toUpper(const char*);
}
