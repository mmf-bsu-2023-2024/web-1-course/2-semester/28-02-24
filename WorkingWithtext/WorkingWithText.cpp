#include <iostream>
#include"Strings.h"

using namespace std;
using namespace StringExtension;

int compareByIncreaseLength(const char* str1, const char* str2);
int compareByDecreaseLength(const char* str1, const char* str2);
int compareText(const char* str1, const char* str2);

int main()
{
	Comparer comparer = strcmp;
	const char* symbols = "	1234567890-=!@#$%^&*()_+{}|][:;'<>?/., \x22";
	const char* source = "The  -98 standard chunk,, 987 of Lorem Ipsum used since the 1500 is reproduced below for those interested.";
	int n = 0;
	char** words = obtainWords(source, n, symbols);
	cout << endl << "Words: " << endl;
	displayWords(words, n);
	sortingWords(words, n, comparer);
	cout << endl << "Sorted words: " << endl;
	displayWords(words, n);
	comparer = compareByIncreaseLength;
	sortingWords(words, n, comparer);
	cout << endl << "Sorted words: " << endl;
	displayWords(words, n);
	comparer = compareByDecreaseLength;
	sortingWords(words, n, comparer);
	cout << endl << "Sorted words: " << endl;
	displayWords(words, n);
	comparer = compareText;
	sortingWords(words, n, comparer);
	cout << endl << "Sorted words: " << endl;
	displayWords(words, n);
	system("pause");
	return 0;
}

int compareByIncreaseLength(const char* str1, const char* str2)
{
	return strlen(str1) - strlen(str2);
}

int compareByDecreaseLength(const char* str1, const char* str2)
{
	return strlen(str2) - strlen(str1);
}

int compareText(const char* str1, const char* str2)
{
	return strcmp(toUpper(str1), toUpper(str2));
}

//Words:
//1. The
//2. standard
//3. chunk
//4. of
//5. Lorem
//6. Ipsum
//7. used
//8. since
//9. the
//10. is
//11. reproduced
//12. below
//13. for
//14. those
//15. interested
//
//Sorted words :
//1. Ipsum
//2. Lorem
//3. The
//4. below
//5. chunk
//6. for
//7. interested
//8. is
//9. of
//10. reproduced
//11. since
//12. standard
//13. the
//14. those
//15. used
//
//Sorted words :
//1. is
//2. of
//3. The
//4. for
//5. the
//6. used
//7. Ipsum
//8. Lorem
//9. below
//10. chunk
//11. since
//12. those
//13. standard
//14. interested
//15. reproduced
//
//Sorted words :
//1. interested
//2. reproduced
//3. standard
//4. Ipsum
//5. Lorem
//6. below
//7. chunk
//8. since
//9. those
//10. used
//11. The
//12. for
//13. the
//14. is
//15. of
//
//Sorted words :
//1. below
//2. chunk
//3. for
//4. interested
//5. Ipsum
//6. is
//7. Lorem
//8. of
//9. reproduced
//10. since
//11. standard
//12. The
//13. the
//14. those
//15. used
